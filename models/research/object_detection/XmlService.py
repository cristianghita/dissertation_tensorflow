import xml.etree.cElementTree as ET


def main():
    frames_dict = {}
    for i in range(299):
        val = 0
        if i <= 223:
            val = 2
        else:
            val = 0
        frames_dict[i] = val
    create_xml_for_video('', frames_dict, video_name='lq_c_sb', fps=999)


def create_xml_for_video(detector, frames_dict, video_name, fps):
    root = ET.Element('root')
    ET.SubElement(root, 'fps').text = str(fps)
    video = ET.SubElement(root, video_name, detector=detector)

    for key, value in frames_dict.items():
        ET.SubElement(video, 'frame', count=str(key)).text = str(value)

    tree = ET.ElementTree(root)
    tree.write('xml/' + detector + '-' + video_name + '.xml')


def parse_xml(filename, video_name):
    root = ET.parse(filename).getroot()
    frames_dict = {}
    for type_tag in root.findall(video_name + '/frame'):
        frame_count = int(type_tag.get('count'))
        frames_dict[frame_count] = int(type_tag.text)
    fps = root.find('fps').text
    return frames_dict, fps


if __name__ == '__main__':
    main()
